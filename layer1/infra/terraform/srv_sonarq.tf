resource "openstack_compute_instance_v2" "sonarq" {
count = "${var.num-sonarq-servers}"
name = "${format("${var.cloud-name}-sonarq%02d", count.index )}"
image_name = "${var.default-image-type}"
flavor_name = "${var.default_flavor}"
security_groups = ["allow_everything-${var.cloud-name}-sec"]
key_pair = "jim-key"
floating_ip = "${openstack_compute_floatingip_v2.sonarq_fip.address}"

metadata {
    ssh_user = "centos"
    role = "role_sonarq"
  }

  depends_on = ["openstack_compute_instance_v2.consul0"]



network {
  name = "${var.infra-net-name}"
}

    connection {
        type = "ssh"
        user = "centos"
       timeout = "${var.ssh-timeout}"
        agent = "true"
      }


# Wait until hostname is correct
    provisioner "remote-exec" {
       inline = [
       "echo ===========================",
       "echo WAIT UNTIL HOSTNAME CORRECT",
       "echo ===========================",
       "set -e",
       "set -x",

       "while [[ $(hostname) != '${format("${var.cloud-name}-sonarq%02d", count.index )}' ]] ; do sleep 5; echo Waiting for hostname to be set to ${format("${var.cloud-name}-sonarq%02d", count.index )}, currently $(hostname) ;  done"
       ]
     }


     provisioner "remote-exec" {
        inline = [
        "echo ===========================",
        "echo ANSIBLE and OTHER UTILS INSTALL",
        "echo ===========================",
        "set -e",
        "set -x",
        "sudo yum  -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm",
        "sudo yum -y -q install deltarpm",
        /*"sudo yum -y -q install unzip wget ansible"*/
       ]
       }

     #Ensure hostname is what you want it to be
     /*provisioner "remote-exec" {
        inline = [
        "echo ===========================",
        "echo SETTING HOSTNAME",
        "echo ===========================",
        "set -e",
        "set -x",

        /*"sudo sh -c 'echo hostname: ${format("${var.cloud-name}-sonarq%02d", count.index )} > /etc/cloud/cloud.cfg.d/99_hostname.cfg'",

        "sudo sh -c 'echo fqdn: ${format("${var.cloud-name}-sonarq%02d", count.index )}.test-rig.net >> /etc/cloud/cloud.cfg.d/99_hostname.cfg'",

        "sudo sh -c 'echo  $(facter ipaddress_eth0) ${format("${var.cloud-name}-sonarq%02d", count.index )}.test-rig.net ${format("${var.cloud-name}-sonarq%02d", count.index )} >> /etc/hosts'",/*
              ]
      }*/


}
