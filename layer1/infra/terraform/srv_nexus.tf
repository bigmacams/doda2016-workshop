resource "openstack_compute_instance_v2" "nexus" {
count = "${var.num-nexus-servers}"
name = "${format("${var.cloud-name}-nexus%02d", count.index )}"
image_name = "${var.default-image-type}"
flavor_name = "${var.default_flavor}"
security_groups = ["allow_everything-${var.cloud-name}-sec"]
key_pair = "jim-key"
floating_ip = "${openstack_compute_floatingip_v2.nexus_fip.address}"

metadata {
    ssh_user = "centos"
    role = "role_nexus"
  }

  /*depends_on = ["openstack_compute_instance_v2.consul0"]*/



network {
  name = "${var.infra-net-name}"
}

connection {
    type = "ssh"
    user = "centos"
   timeout = "${var.ssh-timeout}"
    agent = "true"
  }


# Wait until hostname is correct
    provisioner "remote-exec" {
       inline = [
       "echo ===========================",
       "echo WAIT UNTIL HOSTNAME CORRECT",
       "echo ===========================",
       "set -e",
       "set -x",

       "while [[ $(hostname) != '${format("${var.cloud-name}-nexus%02d", count.index )}' ]] ; do sleep 5; echo Waiting for hostname to be set to ${format("${var.cloud-name}-nexus%02d", count.index )}, currently $(hostname) ;  done"
       ]
     }

     provisioner "remote-exec" {
        inline = [
        "echo ===========================",
        "echo ANSIBLE and OTHER UTILS INSTALL",
        "echo ===========================",
        "set -e",
        "set -x",
        "sudo yum  -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm",
        "sudo yum -y -q install deltarpm"
       ]
       }

}
