# Create Bastion Host
resource "openstack_compute_instance_v2" "bastion" {
  count = "1"
  name = "bastion"
  image_name = "${var.default-image-type}"
  flavor_name = "${var.default_flavor}"
  /*flavor_name = "m1.medium"*/

  security_groups = ["allow_everything-${var.cloud-name}-sec"]
  key_pair = "my-key"
  floating_ip = "${openstack_networking_floatingip_v2.bastion_fip.address}"


  metadata {
      ssh_user = "centos"
      role = "role_consul_client"
    }



#lifecycle {
#  prevent_destroy = "true"
#}


  network {
    name = "${var.infra-net-name}"
    /*fixed_ip_v4 = "10.10.10.10"*/
  }

  connection {
      type = "ssh"
      user = "centos"
     timeout = "${var.ssh-timeout}"
      agent = "true"
      /*bastion_host = "${openstack_networking_floatingip_v2.bastion_fip.address}"*/
      #bastion_host = "bastion.test-rig.net"
    }


  /*provisioner "remote-exec" {
     inline =
     [
       "set -e",
       "set -x",
       "sudo yum -y -q install telnet wget git unzip",
       "sudo mkdir /usr/local/terraform",
       "sudo su -c 'cd /usr/local/terraform ; wget https://releases.hashicorp.com/terraform/0.6.11/terraform_0.6.11_linux_amd64.zip ; unzip terraform_0.6.11_linux_amd64.zip'",
       "echo export PATH=$PATH:/usr/local/terraform >> /home/centos/.bash_profile"
     ]
   }*/




/*hosts:
10.10.10.10 bastion
10.10.10.11 jmaster
10.10.10.12 gitlab
10.10.10.13 nexus
10.10.10.14 rdpbastion
10.10.10.15 consulp

*/



}


 /*"https://onedrive.live.com/download?cid=6B36F53DA423EE5D&resid=6B36F53DA423EE5D%2113908&authkey=AIW9To_FBjsgvIY"*/
