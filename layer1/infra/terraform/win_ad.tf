resource "openstack_compute_instance_v2" "win_ad" {
  count = "${var.num-win_ad-servers}"
  name = "${format("${var.cloud-name}-win_ad%02d", count.index )}"
  image_name = "${var.default-win-image}"
  flavor_name = "m1.medium"
  security_groups = ["allow_everything-${var.cloud-name}-sec"]
  key_pair = "jim-key"

  depends_on = ["openstack_compute_instance_v2.consul0"]

  floating_ip = "${openstack_networking_floatingip_v2.win_ad_fip.address}"

  network {
    name = "${var.infra-net-name}"
    fixed_ip_v4 = "10.0.0.13"
  }

  user_data = "${file("win_ad_userdata.ps1")}"


}
