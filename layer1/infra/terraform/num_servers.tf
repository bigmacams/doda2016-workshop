
variable "num-sonarq-servers" {
  description = ""
  default = "1"
}

variable "num-jmaster-servers" {
  description = ""
  default = "1"
}

variable "num-jslavelx-servers" {
  description = ""
  default = "1"
}

variable "num-jslavewin-servers" {
  description = ""
  default = "1"
}

variable "num-consul0-servers" {
  description = "Number of Consul Primaries"
  default = "1"
}

variable "num-consul1-servers" {
  description = "Number of Consul Primaries"
  default = "1"
}

variable "num-consul2-servers" {
  description = "Number of Consul Primaries"
  default = "1"
}

variable "num-php-servers" {
  description = "Number of Consul Primaries"
  default = "0"
}

variable "num-artifactory-servers" {
  description = "Number of artifacory servers"
  default = "1"
}
variable "num-nexus-servers" {
  description = "Number of nexus servers"
  default = "0"
}

variable "num-win_ad-servers" {
  description = "num-ad-servers"
  default = "0"
}

variable "num-win_console-servers" {
  description = ""
  default = "0"
}

variable "num-gitlab-servers" {
  description = "Number of Consul replicas"
  default = "0"
}

variable "num-log-servers" {
  description = ""
  default = "1"
}
