#Create floating IPs:

resource "openstack_networking_floatingip_v2" "bastion_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_networking_floatingip_v2" "jmaster_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "artifactory_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "nexus_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "sonarq_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "consul0_fip" {
  /*count = "${var.num-consulp-servers}"*/
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "consul1_fip" {
  /*count = "${var.num-consulp-servers}"*/
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "consul2_fip" {
  /*count = "${var.num-consulp-servers}"*/
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_networking_floatingip_v2" "win_ad_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_networking_floatingip_v2" "win_console_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}

resource "openstack_compute_floatingip_v2" "php_fip" {
  region = ""
  pool = "${var.my-ip-pool}"
}
