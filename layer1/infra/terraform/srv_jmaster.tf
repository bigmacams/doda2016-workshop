resource "openstack_compute_instance_v2" "jmaster" {
count = "${var.num-jmaster-servers}"
name = "${format("${var.cloud-name}-jmaster%02d", count.index )}"
image_name = "${var.default-image-type}"
flavor_name = "${var.default_flavor}"
security_groups = ["allow_everything-${var.cloud-name}-sec"]
key_pair = "my-key"
floating_ip = "${openstack_networking_floatingip_v2.jmaster_fip.address}"

metadata {
    ssh_user = "centos"
    role = "role_jmaster"
  }

/*depends_on = ["openstack_compute_instance_v2.consul0"]*/

  network {
    name = "${var.infra-net-name}"
  }

    connection {
        type = "ssh"
        user = "centos"
       timeout = "${var.ssh-timeout}"
        agent = "true"
        /*bastion_host = "${openstack_networking_floatingip_v2.bastion_fip.address}"*/
      }

      /*provisioner "file" {
         source = "../layer2/puppet_modules.bash"
         destination = "/tmp/puppet_modules.bash"
       }

       provisioner "file" {
          source = "../layer2/init.pp"
          destination = "/tmp/init.pp"
        }*/

# Wait until hostname is correct
    provisioner "remote-exec" {
       inline = [
       "echo ===========================",
       "echo WAIT UNTIL HOSTNAME CORRECT",
       "echo ===========================",
       "set -e",
       "set -x",

       "while [[ $(hostname) != '${format("${var.cloud-name}-jmaster%02d", count.index )}' ]] ; do sleep 5; echo Waiting for hostname to be set to ${format("${var.cloud-name}-jmaster%02d", count.index )} ;  done"
       ]
     }

     # Set facter variables
     /*provisioner "remote-exec" {
        inline = [
        "echo ===========================",
        "echo SET FACTER VARS",
        "echo ===========================",

        "set -e",
        "set -x",
        "sudo mkdir -p /etc/facter/facts.d",
        "sudo sh -c 'echo --- > /etc/facter/facts.d/config.yaml'",
        "sudo sh -c 'echo color: ${var.cloud-name} >> /etc/facter/facts.d/config.yaml'",
        /*"sudo sh -c 'echo role: net.test-rig.jmaster000 >> /etc/facter/facts.d/test-rig-config.yaml'",
        "echo successfully set facts color=${var.cloud-name} "
        ]
      }*/

     provisioner "remote-exec" {
        inline = [

        "echo ===========================",
        "echo PUPPET INSTALL, MODULES INSTALL, MANIFEST COPY",
        "echo ===========================",
        "set -e",
        "set -x",
        "sudo yum  -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm",
          "sudo yum -y -q install wget ansible",
          /*"sudo bash /tmp/puppet_modules.bash",*/
          /*"sudo sh -c 'cd / ; tar xvzf /tmp/puppet_modules.tgz'",*/
          /*"sudo mkdir -p /etc/puppet/manifests",*/
          /*"sudo cp /tmp/init.pp /etc/puppet/manifests",*/
          /*"sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo",*/
          /*"sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo",*/
          /*"sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key",*/
          /*"sudo yum -y -q install java jenkins",*/
          /*"sudo systemctl start jenkins"*/
       ]
       }

     #Ensure hostname is what you want it to be
     /*provisioner "remote-exec" {
        inline = [
        "echo ===========================",
        "echo SETTING HOSTNAME",
        "echo ===========================",

        "set -e",
        "set -x",

        "sudo sh -c 'echo hostname: ${format("${var.cloud-name}-jmaster%02d", count.index )} > /etc/cloud/cloud.cfg.d/99_hostname.cfg'",

        "sudo sh -c 'echo fqdn: ${format("${var.cloud-name}-jmaster%02d", count.index )}.test-rig.net >> /etc/cloud/cloud.cfg.d/99_hostname.cfg'",

        "sudo sh -c 'echo  $(facter ipaddress_eth0) ${format("${var.cloud-name}-jmaster%02d", count.index )}.test-rig.net ${format("${var.cloud-name}-jmaster%02d", count.index )} >> /etc/hosts'",
              ]
      }*/

      /*provisioner "remote-exec" {
         inline = [
         "echo ===========================",
         "echo PUPPET APPLYING",
         "echo ===========================",
         "set -e",
         "set -x",
         "sudo puppet apply --modulepath /etc/puppet/modules /etc/puppet/manifests --debug",
         "sudo cat /var/lib/jenkins/secrets/initialAdminPassword"

        ]
        }*/


}
