resource "openstack_compute_instance_v2" "consul0" {
count = "${var.num-consul0-servers}"
#name = "${format("${var.cloud-name}-consul0%02d", count.index )}"
name = "${var.cloud-name}-consul0"
image_name = "${var.default-image-type}"
flavor_name = "${var.default_flavor}"
security_groups = ["allow_everything-${var.cloud-name}-sec"]
key_pair = "my-key"
floating_ip = "${openstack_compute_floatingip_v2.consul0_fip.address}"

metadata {
    ssh_user = "centos"
    role = "role_consul_server_primary"
  }

network {
  name = "${var.infra-net-name}"
  fixed_ip_v4 = "10.0.0.10"
}

    connection {
        type = "ssh"
        user = "centos"
       timeout = "${var.ssh-timeout}"
        agent = "true"
      }


      provisioner "remote-exec" {
         inline = [

         "echo ===========================",
         "echo PUPPET INSTALL, MODULES INSTALL, MANIFEST COPY",
         "echo ===========================",
         "set -e",
         "set -x",
          "sudo yum  -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
        ]
        }




}
