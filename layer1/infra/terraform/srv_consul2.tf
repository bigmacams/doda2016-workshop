resource "openstack_compute_instance_v2" "consul2" {
count = "${var.num-consul2-servers}"
#name = "${format("${var.cloud-name}-consul2%02d", count.index )}"
name = "${var.cloud-name}-consul2"
image_name = "${var.default-image-type}"
flavor_name = "${var.default_flavor}"
security_groups = ["allow_everything-${var.cloud-name}-sec"]
key_pair = "my-key"
floating_ip = "${openstack_compute_floatingip_v2.consul2_fip.address}"


/*depends_on = ["openstack_compute_instance_v2.consul0"]*/


metadata {
    ssh_user = "centos"
    role = "role_consul_server_secondary"
  }

network {
  name = "${var.infra-net-name}"
  fixed_ip_v4 = "10.0.0.12"
}

    connection {
        type = "ssh"
        user = "centos"
       timeout = "${var.ssh-timeout}"
        agent = "true"
      }

      provisioner "remote-exec" {
         inline = [

         "echo ===========================",
         "echo PUPPET INSTALL, MODULES INSTALL, MANIFEST COPY",
         "echo ===========================",
         "set -e",
         "set -x",
          "sudo yum  -y -q install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
        ]
        }


}
