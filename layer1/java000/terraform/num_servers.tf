
variable "num-jboss-servers" {
  description = ""
  default = "3"
}

variable "num-mariadb-servers" {
  description = ""
  default = "1"
}

variable "num-haproxy-servers" {
  description = ""
  default = "1"
}
