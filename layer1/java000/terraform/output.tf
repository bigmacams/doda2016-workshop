/*output "ip" {
    value = "${openstack_compute_instance_v2.jboss.0}"
    }*/

output "jboss_name" {
  value = "${join(",",openstack_compute_instance_v2.jboss.*.name)}"
  }

output "jboss_ip" {
  value = "${join(",",openstack_compute_instance_v2.jboss.*.access_ip_v4)}"
  }

output "mariadb_name" {
  value = "${join(",",openstack_compute_instance_v2.mariadb.*.name)}"
  }

output "mariadb_ip" {
  value = "${join(",",openstack_compute_instance_v2.mariadb.*.access_ip_v4)}"
  }



/*access_ip_v4 = 10.0.0.108
name = red-jboss00*/
