# Config file for INFRA network

variable "cloud-name" {
  description = "Cloud name"
  default = "maroon"
}

variable "SelfServiceSprint" {
  description = "Cloud name"
  default = "maroon"
}

variable "infra-net-name" {
  description = "Cloud name"
  default = "private"
}

variable "default_flavor" {
  description = "Defautl Flavour"
  default = "m1.medium"
}

variable "bastion_host" {
  description = "Defautl Flavour"
  default = "172.24.4.123"
}

/*variable "cloud_pw" {
  description = "Cloud Password"
  default = "EXHcasRfOUu6XUIdbw5NkSFnzbl9fB5WFIrxh4"
}

variable "cloud_keystone" {
  description = "keystone DNSname/ip addr"
  #default = "keystone.uc.fairbanks.nl"
  default = "beast01.test-rig.net"
}*/

variable "cloud_pw" {
  description = "Cloud Password"
  default = "a0cae58c83614224"
}

variable "cloud_keystone" {
  description = "keystone DNSname/ip addr"
  /*default = "beast01.test-rig.net"*/
  default = "147.75.205.39"
}

variable "bastion-host" {
  description = "bastion"
  default = "172.24.4.228"
}



variable "ssh-timeout" {
  description = "ssh timeout"
  default = "120s"
}

variable "default-image-type" {
  description = "serve image to use for instance"
  default = "CentOS7"
}

variable "default-win-image" {
  description = "serve image to use for instance"
  default = "Win2012r2"
}

variable "my-toolbox-yums" {
  description = "serve image to use for instance"
  default = "puppet telnet bind-utils mlocate tcpdump crudini wget unzip"
}

variable "my-dns-server" {
  description = "DNS Server - update for appropriate environment"
  #SuperMicro
  #default = "192.168.10.12"
  #Fuel
  default = "8.8.8.8"
  }

variable "my-proxy" {
  description = "DNS Server - update for appropriate environment"
  #SuperMicro
  #default = "192.168.10.12"
  #Fuel
  default = "10.7.7.189"
  }

variable "my-keystone" {
  description = "Horizon/Keystone Server - update for appropriate environment"
  #SuperMicro
  #default = "192.168.10.10"
  #Fuel
  default = "147.75.195.183"
  }

variable "my-ip-pool" {
  description = "IP Pool name - update for appropriate environment"
  #SuperMicro
  #default = "external-network"
  #Fuel
  default = "public"
  }
