provider "consul" {
    address = "10.0.0.10:8600"
    datacenter = "doda2016_dc"
}
