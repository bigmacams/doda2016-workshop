# Configure the OpenStack Provider

provider "openstack" {
    user_name  = "${var.cloud-name}"
    tenant_name = "${var.cloud-name}"
    password  = "${var.cloud_pw}"
    auth_url  = "http://${var.cloud_keystone}:5000/v2.0"
    insecure = true
}

#TF_VAR_cloud_keystone=192.168.0.30
#TF_VAR_cloud_name=maroon
#TF_VAR_cloud_pw=DODA2016
