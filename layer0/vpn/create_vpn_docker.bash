#!/bin/bash


sudo tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/$releasever/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

sudo yum -y update
sudo yum -y install epel-release
sudo yum -y install openvpn easy-rsa
sudo yum -y install docker-engine
sudo systemctl enable docker
sudo systemctl start docker
