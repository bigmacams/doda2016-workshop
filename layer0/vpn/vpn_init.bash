#!/bin/bash
OVPN_DATA="ovpn-data"
docker run --name $OVPN_DATA -v /etc/openvpn busybox
docker run --volumes-from $OVPN_DATA --rm kylemanna/openvpn ovpn_genconfig -u udp://vpn.test-rig.net
docker run --volumes-from $OVPN_DATA --rm -it kylemanna/openvpn ovpn_initpki
docker run --volumes-from $OVPN_DATA -d -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn

while read USER; do
  echo docker run --volumes-from $OVPN_DATA --rm -it kylemanna/openvpn easyrsa build-client-full ${USER} nopass
  echo docker run --volumes-from $OVPN_DATA --rm kylemanna/openvpn ovpn_getclient ${USER} \> ${USER}.ovpn
done <users.txt
