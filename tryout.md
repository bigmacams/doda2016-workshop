##Infra


###Terraform




###Ansible




#####Get Consul DNS working

-consul0
- edit ```/etc/dnsmasq.d/10-consul```
replace text with
```server=/consul/127.0.0.1#8600```

- edit ```/etc/dnsmasq.conf```
replace text ```interface=docker0``` with ```interface=eth0```


```sh
sudo bash
systemctl stop consul
systemctl restart dnsmasq
rm -rf /tmp/consul  
/opt/consul/bin/consul agent -server -bootstrap  -data-dir /tmp/consul
```


-consul1
- edit ```/etc/dnsmasq.d/10-consul```
replace text with
```server=/consul/127.0.0.1#8600```

- edit ```/etc/dnsmasq.conf```
replace text ```interface=docker0``` with ```interface=eth0```


```sh
sudo bash
systemctl stop consul
systemctl restart dnsmasq
rm -rf /tmp/consul  
/opt/consul/bin/consul agent -server  -data-dir /tmp/consul
```


-consul2
- edit ```/etc/dnsmasq.d/10-consul```
replace text with
```server=/consul/127.0.0.1#8600```

- edit ```/etc/dnsmasq.conf```
replace text ```interface=docker0``` with ```interface=eth0```

```sh
sudo bash
systemctl stop consul
systemctl restart dnsmasq
rm -rf /tmp/consul
/opt/consul/bin/consul agent -server  -data-dir /tmp/consul
```

-consul0
```sh
sudo bash
/opt/consul/bin/consul join 10.0.0.11 10.0.0.12
ctrl-c (stop running server on consul0)
consul agent -server  -data-dir /tmp/consul
/opt/consul/bin/consul join 10.0.0.11
```


-test working
```sh
dig @localhost -p 8600 maroon-consul2.node.consul ANY
dig @localhost maroon-consul2.node.consul ANY
dig @localhost www.nu.nl ANY
```
